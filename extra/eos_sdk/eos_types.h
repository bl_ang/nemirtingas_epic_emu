// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once

#include "eos_common.h"

#define EOS_Platform_Options EOS_Platform_Options007

#include "eos_types1.6.0.h"
#include "eos_types1.5.0.h"
#include "eos_types1.2.0.h"
#include "eos_types1.0.0.h"

#define EOS_PLATFORM_OPTIONS_API_LATEST EOS_PLATFORM_OPTIONS_API_007